from mitmproxy import http
import requests
import json

initial_url = requests.get("https://reqres.in/api/users?page=2")
initial_url_dict = json.loads(initial_url.content)

value_replace = [("Oscar", "Cordoba"), ("Hugo", "Ibarra"), ("Jorge","Bermudez"), ("Walter", "Samuel"), ("Rodolfo", "Arruabarrena"), ("Mauricio", "Serna")]

for first_replace_names, first_json_names in zip(value_replace, initial_url_dict['data']):
    first_json_names['first_name'] = first_replace_names[0]
    first_json_names['last_name'] = first_replace_names[1]

response_url = json.dumps(initial_url_dict)

print(response_url)

