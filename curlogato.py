from mitmproxy import http
import re
import json


def request(flow):
    print(flow)


def response(flow):
    if re.findall('/api/users', flow.request.path):
        response = json.loads(flow.response.text)
        replaces = {7: "Han Solo", 8: "Leia", 9: "Chewaca", 10: "Darth Vader", 11: "R2D2", 12: "C3PO"}

        for userData in response["data"]:
            userData["first_name"] = replaces[userData["id"]]
        flow.response.text = json.dumps(response)